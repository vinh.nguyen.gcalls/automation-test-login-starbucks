const puppeteer = require('puppeteer');
const chai = require('chai');

describe('Test login page StarBucks', () => {
    let browser;
    let page;
    const email = 'vinhnguyen03091999@gmail.com';
    const password = 'PhucVinh39@!';
    const proxyUrl = 'http://64.225.8.121:9994';

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null,
            args: [`--proxy-server=${proxyUrl}`],
        });
        page = await browser.newPage();
    }
    );

    beforeEach(async () => {
        // Before each test we open a new page.
        page = await browser.newPage();
        await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36');
        await page.goto('https://app.starbucks.com/account/signin');
    }
    );

    it('Login successfully', async () => {
        await page.type('#username', email);
        await page.type('#password', password);
        await page.click("button[type='submit']");
        expect(title).to.equal("Home: Starbucks Coffee Company");
    }
    );

    afterEach(async () => {
        await page.close();
    }
    );

    after(async () => {
        await browser.close();
    }
    );

});
