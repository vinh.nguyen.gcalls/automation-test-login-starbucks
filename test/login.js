const puppeteer = require('puppeteer');
const expect = require('chai').expect;

describe('Test login page StarBucks', () => {
    let browser;
    let page;
    const email = 'vinhnguyen03091999@gmail.com';
    const password = 'PhucVinh39@!';

    before(async () => {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null,
        });
        page = await browser.newPage();
        await page.setDefaultTimeout(10000);
        await page.setDefaultNavigationTimeout(20000);
    }
    );

    beforeEach(async () => {
        // Before each test we open a new page.
        page = await browser.newPage();
        await page.goto('https://app.starbucks.com/account/signin');
    }
    );

    it('Enter page successfully', async () => {
        const title = await page.title();
        await page.waitForSelector('#consent_blackbar');
        await page.click('#truste-consent-button');
        expect(title).to.equal("Account sign in: Starbucks Coffee Company");
    }
    );

    it('Enter empty email and password', async () => {
        await page.waitForSelector('#consent_blackbar');
        await page.click('#truste-consent-button');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#username-validationHint span").innerText == "Error:\\nEnter an email/username."');
        await page.waitForFunction('document.querySelector("#password-validationHint span").innerText == "Error:\\nEnter a password."');
    }
    );

    it('Enter empty email', async () => {
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#username-validationHint span").innerText == "Error:\\nEnter an email/username."');
    }
    );

    it('Enter empty password', async () => {
        await page.type('#username', email);
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector("#password-validationHint span").innerText == "Error:\\nEnter a password."');
    }
    );

    it('Enter email without domain', async () => {
        await page.type('#username', 'vinhnguyen03091999');
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter email without Domain part', async () => {
        await page.type('#username', 'vinhnguyen03091999@');
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter email without Local part', async () => {
        await page.type('#username', '@gmail.com');
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter email without @', async () => {
        await page.type('#username', 'vinhnguyen03091999gmail.com');
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter email without Local part and @', async () => {
        await page.type('#username', 'gmail.com');
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Email with invalid Domain', async () => {
        await page.type('#username', 'vinhnguyen03091999@gmail');
        await page.type('#password', 'phucvinh39');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with less than 8 characters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with more than 25 characters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456789901234567890123456');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456789');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only special characters', async () => {
        await page.type('#username', email);
        await page.type('#password', '!@#$%^&*()_+');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only uppercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', 'ABCDEFGHI');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', 'abcdefghi');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers and special characters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456!@#$%^&*()_+');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers and uppercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456789ABCD');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456789abcd');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only special characters and uppercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '!@#$%^&*()_+ABCD');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only special characters and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '!@#$%^&*()_+abcd');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only uppercase letters and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', 'ABCDEFabcdef');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers, special characters and uppercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456!@()_+ABC');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers, special characters and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456!@()_+abc');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only numbers, uppercase letters and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123456ABCabc');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with only special characters, uppercase letters and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '!@#$%^&*()_+ABCabc');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );

    it('Enter password with numbers, special characters, uppercase letters and lowercase letters', async () => {
        await page.type('#username', email);
        await page.type('#password', '123!@#$ABCabc');
        await page.click("button[type='submit']");
        await page.waitForFunction('document.querySelector(".sb-expander p.mt2").innerText == "The email or password you entered is not valid. Please try again."');
    }
    );


    afterEach(async () => {
        await page.close();
    }
    );

    after(async () => {
        await browser.close();
    }
    );
});